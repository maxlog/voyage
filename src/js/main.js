$(function () {
    $('.main-slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: false,
      infinite: true,
      prevArrow: $('.slider__arrow-prev'),
      nextArrow: $('.slider__arrow-next'),
});
});